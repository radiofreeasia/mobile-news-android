package at.xtools.pwawrapper;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.webkit.ProxyConfig;
import androidx.webkit.ProxyController;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.clostra.newnode.NewNode;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.net.MalformedURLException;
import java.util.concurrent.Executor;

import at.xtools.pwawrapper.ui.UIManager;
import at.xtools.pwawrapper.webview.WebViewHelper;
import info.guardianproject.netcipher.NetCipher;
import info.guardianproject.netcipher.proxy.OrbotHelper;
import info.guardianproject.netcipher.proxy.StatusCallback;

public class MainActivity extends AppCompatActivity {
    // Globals
    private UIManager uiManager;
    private WebViewHelper webViewHelper;
    private boolean intentHandled = false;

    private boolean mUseNewNode = false;
    private boolean mEnableCircumvention = false;
    private boolean mUseTor = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Setup Theme
        setTheme(R.style.AppTheme_NoActionBar);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final String appKey = "appUrl";
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        String appUrl = "";

        if (!prefs.getString("appUrl","").isEmpty())
            appUrl = prefs.getString(appKey, "");
        else {
            //randomly choose from one of the feeds
            String[] mirrors = getResources().getStringArray(R.array.app_urls);
            int urlIdx = (int)(Math.random()*mirrors.length);
            appUrl = mirrors[urlIdx] + getString(R.string.app_url_append);
        }

        // Setup Helpers
        uiManager = new UIManager(this, appUrl);
        webViewHelper = new WebViewHelper(this, uiManager, appUrl);

        // Setup App
        webViewHelper.setupWebView();
        //     uiManager.changeRecentAppsIcon();

        // Check for Intents
        try {
            Intent i = getIntent();
            String intentAction = i.getAction();
            // Handle URLs opened in Browser
            if (!intentHandled && intentAction != null && intentAction.equals(Intent.ACTION_VIEW)) {
                Uri intentUri = i.getData();
                String intentText = "";
                if (intentUri != null) {
                    intentText = intentUri.toString();
                }
                // Load up the URL specified in the Intent
                if (!intentText.equals("")) {
                    intentHandled = true;
                    webViewHelper.loadIntentUrl(intentText);
                }
            } else {


                if (TextUtils.isEmpty(appUrl)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Enter Test URL");

// Set up the input
                    final EditText input = new EditText(this);
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                    input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_URI);
                    builder.setView(input);

// Set up the buttons
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            try {
                                String appUrl = input.getText().toString();
                                webViewHelper.setAppUrl(appUrl);
                                webViewHelper.loadHome();

                                SharedPreferences.Editor editor = prefs.edit();
                                editor.putString(appKey, appUrl);
                                editor.commit();

                            } catch (MalformedURLException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });

                    builder.show();
                } else {
                    // Load up the Web App
                    webViewHelper.setAppUrl(appUrl);
                    webViewHelper.loadHome();
                }
            }
        } catch (Exception e) {
            // Load up the Web App
            webViewHelper.loadHome();
        }

        findViewById(R.id.fab).setOnClickListener(view -> showProxyDialog ());
        findViewById(R.id.logo).setOnClickListener(view -> showProxyDialog ());

        webViewHelper.getWebView().setOnLongClickListener(view -> {
            showProxyDialog ();
            return false;
        });
    }

    private void showProxyDialog () {
        String[] coptions = {getString(R.string.connection_direct),getString(R.string.connection_mirror),
                getString(R.string.connection_peer),getString(R.string.connection_tor_proxy)};
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(R.string.connection_options)
                .setItems(coptions, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // The 'which' argument contains the index position of the selected item.

                        if (which == 0) {
                            //direct TBD
                            mUseNewNode = false;
                            mUseTor = false;
                            mEnableCircumvention = false;
                        }
                        else if (which == 1) {
                            //mirror
                            mUseNewNode = false;
                            mUseTor = false;
                            mEnableCircumvention = false;
                        }
                        else if (which == 3)
                        {
                            //tor
                            mUseNewNode = false;
                            mUseTor = true;
                            mEnableCircumvention = true;
                        }
                        else if (which == 2)
                        {
                            //newnode
                            mUseNewNode = true;
                            mUseTor = false;
                            mEnableCircumvention = true;
                        }

                        checkCircumvention();
                        webViewHelper.loadHome();
                    }
                });
        builder.create().show();
    }

    private void checkCircumvention () {

        if (mEnableCircumvention) {

            if (mUseTor && OrbotHelper.isOrbotInstalled(this)) {

                //enable proxy status listener
                OrbotHelper.get(this).addStatusCallback(new StatusCallback() {
                    @Override
                    public void onEnabled(Intent statusIntent) {

                    }

                    @Override
                    public void onStarting() {

                    }

                    @Override
                    public void onStopping() {

                    }

                    @Override
                    public void onDisabled() {

                    }

                    @Override
                    public void onStatusTimeout() {

                    }

                    @Override
                    public void onNotYetInstalled() {

                    }
                });

                //enable OrbotHelper

                OrbotHelper.get(this).init();

                //set proxy
                enableTorProxy();
            }
            else  if (mUseNewNode) {
                NewNode.setRequestDiscoveryPermission(false);
                NewNode.setLogLevel(2);
                NewNode.init();
                webViewHelper.setUsingCircumvention("newnode");
            }

        }
        else {
            ProxyController.getInstance().clearProxyOverride(new Executor() {
                @Override
                public void execute(Runnable command) {
                    //do nothing
                }
            }, new Runnable() {
                @Override
                public void run() {

                }
            });
            //other proxy options?
            NewNode.shutdown();
            webViewHelper.setUsingCircumvention("none");
        }

    }

    private void enableTorProxy() {

        NetCipher.setProxy(NetCipher.ORBOT_HTTP_PROXY);
        webViewHelper.setUsingCircumvention("tor");

        ProxyConfig proxyConfig = new ProxyConfig.Builder()
                .addProxyRule("127.0.0.1:8118") //http proxy for tor
                .addDirect().build();
        ProxyController.getInstance().setProxyOverride(proxyConfig, new Executor() {
            @Override
            public void execute(Runnable command) {
                //do nothing
            }
        }, new Runnable() {
            @Override
            public void run() {

            }
        });
    }

    @Override
    protected void onPause() {
        webViewHelper.onPause();
        super.onPause();
    }

    @Override
    protected void onResume() {
        webViewHelper.onResume();
        // retrieve content from cache primarily if not connected
        webViewHelper.forceCacheIfOffline();
        super.onResume();
    }

    // Handle back-press in browser
    @Override
    public void onBackPressed() {
        if (!webViewHelper.goBack()) {
            super.onBackPressed();
        }
    }
}
