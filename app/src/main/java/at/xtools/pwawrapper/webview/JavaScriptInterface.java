package at.xtools.pwawrapper.webview;

import static android.content.Context.DOWNLOAD_SERVICE;

import android.Manifest;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.util.Base64;
import android.webkit.JavascriptInterface;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.app.ShareCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;

import at.xtools.pwawrapper.MainActivity;
import at.xtools.pwawrapper.R;

public class JavaScriptInterface {

    private Activity context;
    public JavaScriptInterface(Activity context) {
        this.context = context;
    }

    @JavascriptInterface
    public Boolean canShare() {
        return true;
    }

    @JavascriptInterface
    public void share(String title, String url) {

        ShareCompat.IntentBuilder.from(context)
                .setType("text/plain")
                .setChooserTitle(title)
                .setText(url)
                .startChooser();
    }

    @JavascriptInterface
    public Boolean canDownload() {
        return true;
    }

    @JavascriptInterface
    public void download(String title, String mimeType, byte[]  mediaBytes) {
        String extension = MimeTypeMap.getSingleton().getExtensionFromMimeType(mimeType);

        final File dwldsPath = new File( context.getCacheDir(), "mobilenews_download_" + new Date().getTime() + "." + extension);
        FileOutputStream os;
        try {
            os = new FileOutputStream(dwldsPath, false);
            os.write(mediaBytes);
            os.flush();
            mediaBytes = null;
            downloadFile(context, title, dwldsPath,mimeType);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @JavascriptInterface
    public Boolean canDownloadUrl() {
        return true;
    }

    @JavascriptInterface
    public void downloadUrl(String title, String mimeType, String mediaUrl) {
        // TODO

        Uri uri = Uri.parse(mediaUrl);
     //   context.registerReceiver(attachmentDownloadCompleteReceive, new IntentFilter(
       //         DownloadManager.ACTION_DOWNLOAD_COMPLETE));

        Toast.makeText(context,context.getString(R.string.downloading) + ": " + title,Toast.LENGTH_LONG).show();

        String fileExt = MimeTypeMap.getSingleton().getExtensionFromMimeType(mimeType);
        String fileName = title + "." + fileExt;

        DownloadManager.Request request = new DownloadManager.Request(uri);
        request.setMimeType(mimeType);
        request.setTitle(fileName);
        request.allowScanningByMediaScanner();
        request.setVisibleInDownloadsUi(true);
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE);
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName);
        DownloadManager dm = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        dm.enqueue(request);

    }

    @JavascriptInterface
    public void getBase64FromBlobData(String base64Data) throws IOException {
        convertBase64StringToFileAndStoreIt(base64Data);
    }
    public static String getBase64StringFromBlobUrl(String blobUrl, String mimeType) {
        if(blobUrl.startsWith("blob")){
            return "javascript: var xhr = new XMLHttpRequest();" +
                    "xhr.open('GET', '"+ blobUrl +"', true);" +
                    "xhr.setRequestHeader('Content-type','" + mimeType + "');" +
                    "xhr.responseType = 'blob';" +
                    "xhr.onload = function(e) {" +
                    "    if (this.status == 200) {" +
                    "        var blobData = this.response;" +
                    "        var reader = new FileReader();" +
                    "        reader.readAsDataURL(blobData);" +
                    "        reader.onloadend = function() {" +
                    "            base64data = reader.result;" +
                    "            Android.getBase64FromBlobData(base64data);" +
                    "        }" +
                    "    }" +
                    "};" +
                    "xhr.send();";
        }
        return "javascript: console.log('It is not a Blob URL');";
    }
    private void convertBase64StringToFileAndStoreIt(String base64Media) throws IOException {

        String mimeType = base64Media.substring(base64Media.indexOf("data:")+5,base64Media.indexOf(";"));
        String extension = MimeTypeMap.getSingleton().getExtensionFromMimeType(mimeType);

        final File dwldsPath = new File( context.getCacheDir(), "mobilenews_download_" + new Date().getTime() + "." + extension);
        byte[] mediaBytes = Base64.decode(base64Media, 0);
        FileOutputStream os;
        os = new FileOutputStream(dwldsPath, false);
        os.write(mediaBytes);
        os.flush();

        sendFile(context, dwldsPath,mimeType);
    }

    public static void sendFile(Context context, File fileMedia, String mimeType) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType(mimeType);
        Uri uri = FileProvider.getUriForFile(context, context.getPackageName() + ".fileprovider", fileMedia);
        intent.putExtra(Intent.EXTRA_STREAM, uri);
        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        // Workaround for Android bug.
        // grantUriPermission also needed for KITKAT,
        // see https://code.google.com/p/android/issues/detail?id=76683
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
            List<ResolveInfo> resInfoList = context.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
            for (ResolveInfo resolveInfo : resInfoList) {
                String packageName = resolveInfo.activityInfo.packageName;
                context.grantUriPermission(packageName, uri, Intent.FLAG_GRANT_READ_URI_PERMISSION);
            }
        }
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
        }
    }

    public static void downloadFile(Activity context, String title, File fileMedia, String mimeType) {

        if(ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED)
        {
            String[] permissionArray = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
            // Permission is not granted
            ActivityCompat.requestPermissions(context,
                    permissionArray,
                    12345);
        }
        else {
            File fileFolderDownloads = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
            File fileDownload = new File(fileFolderDownloads, fileMedia.getName());
            try {
                copy(fileMedia, fileDownload);

                DownloadManager dm = (DownloadManager) context.getSystemService(DOWNLOAD_SERVICE);
                dm.addCompletedDownload(title, title, true, mimeType, fileDownload.getAbsolutePath(), fileMedia.length(), true);

            } catch (IOException e) {
                e.printStackTrace();

            }
        }

    }

    public static void copy(File src, File dst) throws IOException {
        InputStream in = new FileInputStream(src);
        try {
            OutputStream out = new FileOutputStream(dst);
            try {
                // Transfer bytes from in to out
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
            } finally {
                out.close();
            }
        } finally {
            in.close();
        }
    }
}
